#include "ScanTaskImpl.h"

ScanTaskImpl::ScanTaskImpl(int trigPin, int echoPin, int ledPin, OutData* data) {
  this->trigPin = trigPin;
  this->echoPin = echoPin;
  this->data = data;
  this->ledPin = ledPin;
}

void ScanTaskImpl::init(int period) {
  Task::init(period);
  led = new Led(this->ledPin);
  this->sonar = new SonarImpl(this->trigPin, this->echoPin);
}

void ScanTaskImpl::tick() {
  if (this->data->phase == 2) {
    this->distance = sonar->getDistance();
    this->data->distance = this->distance;
    
    if (this->data->distance < 20.0) {
      for (int i = 0; i < 3; i++) {
        this->led->switchOn();
        delay(50);
        this->led->switchOff();
        delay(50);
        Serial.println("cose");
      }
    }
    this->data->phase = 3;
  }
}
