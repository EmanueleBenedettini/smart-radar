#ifndef __POT__
#define __POT__

class Pot { 
public:
  Pot(int pin, unsigned int x, unsigned int y);
  void setMin(unsigned int);
  void setMax(unsigned int); 
  unsigned int getValue();   
private:
  int pin;  
  unsigned int minVal;
  unsigned int maxVal;
};

#endif
