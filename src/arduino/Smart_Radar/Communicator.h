#ifndef __COMMUNICATOR__
#define __COMMUNICATOR__

enum DIRECTION {LEFT = 0, RIGHT = 1, STOP = 3};

enum WORK_MODE {SINGLE = 0, MANUAL = 1, AUTO = 2};

typedef struct {
  unsigned char mode;
  DIRECTION dir;
  unsigned int Tmin;
  unsigned int Tmax;
} InData;

typedef struct {
  unsigned char mode;
  DIRECTION dir;
  unsigned char pos;
  float distance;
  unsigned phase;
} OutData;


class Communicator {

  public:
    void init(unsigned long speed);
    virtual void send(OutData* data);
    virtual int read(InData* data);
    virtual bool available();

  private:
    InData inBuffMem;
};


#endif
