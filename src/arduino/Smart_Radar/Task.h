#ifndef __TASK__
#define __TASK__

class Task {
    unsigned int myPeriod;
    unsigned int timeElapsed;
    bool active;
    bool systemTask;

  public:

    virtual void init(unsigned int period) {
      this->init(period, false);
    }
    virtual void init(unsigned int period, bool systemTask) {
      myPeriod = period;
      timeElapsed = 0;
      this->active = true;
      this->systemTask = systemTask;
    }

    virtual void tick() = 0;

    bool updateAndCheckTime(unsigned int basePeriod) {
      timeElapsed += basePeriod;
      if (timeElapsed >= myPeriod) {
        timeElapsed = 0;
        return true;
      } else {
        return false;
      }
    }

    bool isActive() {
      return active;
    }

    void setActive(bool active) {
      this->active = active;
    }

    bool isSystemTask() {
      return systemTask;
    }

    bool isKillable() {
      return !systemTask;
    }

    void kill(){
        if(isKillable()){
          setActive(false);
          delete(this);
        }
    }
};

#endif
