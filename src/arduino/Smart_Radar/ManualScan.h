#ifndef __MANUAL_SCAN__
#define __MANUAL_SCAN__

#include "Communicator.h"
#include "Task.h"
#include "servo_motor_impl.h"
#include "SonarImpl.h"
#include "Led.h"
#include <arduino.h>

class ManualScan: public Task {
  private:
    InData* inData;
    OutData data = {0, 0, LEFT};
    Communicator *comm;
    ServoMotor* pMotor;
   

    int pos;
    unsigned char passo;
    enum { STOP, ORARIO, ANTIORARIO} state;
    int delta;
    int servoPin;
    bool continuous;
    int trigPin;
    int echoPin;
    Sonar* sonar;
    float distance;

    void rotate();

  public:
    ManualScan( Communicator *comm, InData *inData );
    ManualScan( Communicator *comm, InData *inData, int servoPin, int delta, int trigPin, int echoPin, bool continuous );
    void init(unsigned int period);
    void tick();

};

#endif
