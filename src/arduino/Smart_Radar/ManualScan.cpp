#include "ManualScan.h"
#include "servo_motor.h"
#include "Sonar.h"
#include "Arduino.h"

#define SECTORS 16
#define D_FAR 1.5

#define MOTOR_PIN 4
#define TRIGGER_PIN 7
#define ECHO_PIN 8
#define MAXANGLE 180

ManualScan::ManualScan( Communicator *comm, InData *inData ) {
  ManualScan( comm, inData, MOTOR_PIN, 11, TRIGGER_PIN, ECHO_PIN , false);
}

ManualScan::ManualScan( Communicator *comm, InData *inData, int servoPin, int delta, int trigPin, int echoPin , bool continuous) {
  this->comm = comm;
  this->inData = inData;
  this->servoPin = servoPin;
  this->pos = -delta;
  this->delta = delta;
  this->trigPin = trigPin;
  this->echoPin = echoPin;
  this->continuous = continuous;
}

void ManualScan::init(unsigned int period) {
  Task::init(period);

  this->sonar = new SonarImpl(this->trigPin, this->echoPin);

  this->pMotor = new ServoMotorImpl(this->servoPin);
  this->pMotor->on();
  this->state = ORARIO;
  this->passo = 0;
}

void ManualScan::tick() {

  if (this->passo % 2) { //se dispari effettua misura
    this->distance = sonar->getDistance();
    this->data.distance = this->distance;
    //comm->send(&this->data);
    if (data.distance < D_FAR) {
      Serial.println(String() + data.mode + " " + data.pos + " " + data.distance);
    }
  } else { //se pari muovi il servo
    rotate();
  }

  this->passo++;

  delay(5);
}

void ManualScan::rotate() {
  pMotor->on();
  if (this->inData->dir == LEFT) {
    state = ANTIORARIO;
    this->inData->dir = 3;
  } else if (this->inData->dir == RIGHT) {
    state = ORARIO;
    this->inData->dir = 3;
  } else {
    state = STOP;
  }

  switch (state)  {
    case ANTIORARIO:
      this->pos = this->pos > 0 ? this->pos - this->delta : 0;
      this->state = this->pos == 0 ? ORARIO : ANTIORARIO;
      pMotor->setPosition(this->pos);
      this->data.pos = this->pos;
      break;

    case ORARIO:
      this->pos = this->pos < MAXANGLE ? this->pos + this->delta : MAXANGLE;
      this->state = this->pos == MAXANGLE ? ANTIORARIO : ORARIO;
      pMotor->setPosition(this->pos);
      this->data.pos = this->pos;
      break;
    default:
      break;
  }
}
