#include "Communicator.h"
#include <Arduino.h>

#define BUFFER_SIZE 50
#define N_POS 10

void Communicator::init(unsigned long speed) {
  Serial.begin(speed);
  while (!Serial);
}

void Communicator::send(OutData* data) {
  //Serial.println(data, sizeof(OutData));
  Serial.println(String() + data->mode + " " + data->pos + " " + data->distance);
}

int Communicator::read(InData * data) {
  char buff[BUFFER_SIZE];
  byte posGradient = 180 / N_POS;

  unsigned int l = Serial.readBytesUntil('\n', buff, BUFFER_SIZE);

  if (l > 0 && l < BUFFER_SIZE - 1) {
    buff[l] = 0;
    Serial.println(buff);

    /*
     * Parsing di stringa, piuttosto corposo...
     * salvo le modifiche in cache
     */
    if ((String() + buff).equals("single")) {
      inBuffMem.mode = 0;
    } else if ((String() + buff).equals("manual")) {
      inBuffMem.mode = 1;
    } else if ((String() + buff).equals("auto")) {
      inBuffMem.mode = 2;
    } else if ((String() + buff).equals("<")) {
      inBuffMem.dir = LEFT;
    } else if ((String() + buff).equals(">")) {
      inBuffMem.dir = RIGHT;
    } else if ((String() + buff).startsWith("Tmin ")) {
      char *c = &buff[5];
      inBuffMem.Tmin = (String() + c).toFloat() * 1000;
      //Serial.println(data->Tmin);
    } else if ((String() + buff).startsWith("Tmax ")) {
      char *c = &buff[5];
      inBuffMem.Tmax = (String() + c).toFloat() * 1000;
    } else {
      return -1;
    }
  }

  //assegno i valori in cache a data
  data->mode = inBuffMem.mode;
  data->dir = inBuffMem.dir;
  data->Tmin = inBuffMem.Tmin;
  data->Tmax = inBuffMem.Tmax;

  if (l >= BUFFER_SIZE) {
    return -2;
  }

  return 0;
}

bool Communicator::available() {
  return Serial.available();
}
