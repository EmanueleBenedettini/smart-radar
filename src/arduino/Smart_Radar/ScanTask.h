#ifndef __SCAN__
#define __SCAN__

#include "Communicator.h"
#include "Task.h"
#include "servo_motor_impl.h"
#include "SonarImpl.h"
#include "Led.h"
#include <arduino.h>

class ScanTask: public Task {
  private:
    OutData data = {0, 0, LEFT};
    Communicator *comm;
    ServoMotor* pMotor;
    Led la = new Led(12);
    Led ld = new Led(13);

    int pos;
    unsigned char passo;
    enum { STOP, ORARIO, ANTIORARIO} state;
    int delta;
    int servoPin;
    bool continuous;
    int trigPin;
    int echoPin;
    Sonar* sonar;
    float distance;

    void rotate();

  public:
    ScanTask( Communicator *comm );
    ScanTask( Communicator *comm, int servoPin, int delta, int trigPin, int echoPin, bool continuous );
    void init(unsigned int period);
    void tick();

};

#endif
