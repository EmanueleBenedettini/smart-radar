#include "ScanTask.h"
#include "servo_motor.h"
#include "Sonar.h"
#include "Arduino.h"

#define SECTORS 16
#define D_FAR 1.5

#define MOTOR_PIN 4
#define TRIGGER_PIN 7
#define ECHO_PIN 8
#define MAXANGLE 180

ScanTask::ScanTask( Communicator *comm ) {
  ScanTask( comm, MOTOR_PIN, 11, TRIGGER_PIN, ECHO_PIN , false);
}

ScanTask::ScanTask( Communicator *comm, int servoPin, int delta, int trigPin, int echoPin , bool continuous) {
  this->comm = comm;
  this->servoPin = servoPin;
  this->pos = -delta;
  this->delta = delta;
  this->trigPin = trigPin;
  this->echoPin = echoPin;
  this->continuous = continuous;
}

void ScanTask::init(unsigned int period) {
  Task::init(period);

  this->sonar = new SonarImpl(this->trigPin, this->echoPin);

  this->pMotor = new ServoMotorImpl(this->servoPin);
  this->pMotor->on();
  this->state = ORARIO;
  this->passo = 0;
}

void ScanTask::tick() {

  if (this->passo % 2) { //se dispari effettua misura
    this->distance = sonar->getDistance();
    this->data.distance = this->distance;
    //comm->send(&this->data);
    if (data.distance < D_FAR) {
      Serial.println(String() + data.mode + " " + data.pos + " " + data.distance);
      if(!continuous){
        ld.switchOn();
      }else{
        la.switchOn();
      }
    }else{
      if(!continuous)ld.switchOff();
    }
  } else { //se pari muovi il servo
    rotate();
  }

  this->passo++;
  if (this->passo == SECTORS * 2 ) {
    if (!this->continuous) {
      this->setActive(false);
      //riabilita interrupt
    }
    this->state = this->state == ORARIO? ANTIORARIO:ORARIO ;
    la.switchOff();
  }
  delay(5);
}

void ScanTask::rotate() {
  pMotor->on();
  switch (state)  {
    case ANTIORARIO:
      this->pos = this->pos > 0 ? this->pos - this->delta : 0;
      this->state = this->pos == 0 ? ORARIO : ANTIORARIO;
      pMotor->setPosition(this->pos);
      this->data.pos = this->pos;
      break;

    case ORARIO:
      this->pos = this->pos < MAXANGLE ? this->pos + this->delta : MAXANGLE;
      this->state = this->pos == MAXANGLE ? ANTIORARIO : ORARIO;
      pMotor->setPosition(this->pos);
      this->data.pos = this->pos;
      break;
    default:
      break;
  }
}
