#include "Pot.h"
#include "Arduino.h"

Pot::Pot(int pin, unsigned int x, unsigned int y){
  this->pin = pin;
  pinMode(pin,INPUT);
  setMin(x);
  setMax(y);
}

void Pot::setMin(unsigned int n){
  this->minVal = n;
}

void Pot::setMax(unsigned int n){
  this->maxVal = n;
}

unsigned int Pot::getValue(){
  return map(analogRead(this->pin), 0, 1024, this->minVal, this->maxVal);
};
