#ifndef __SONAR_IMPL__
#define __SONAR_IMPL__

#include <arduino.h>
#include "Sonar.h"

class SonarImpl : public Sonar{
    private:

    int trigPin;
    int echoPin;

    public: 

    SonarImpl(int trigPin, int echoPin);
    float getDistance();    
};
#endif
