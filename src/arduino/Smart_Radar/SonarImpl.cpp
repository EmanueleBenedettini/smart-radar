#include "SonarImpl.h"
#include "Arduino.h"

SonarImpl::SonarImpl(int trigPin, int echoPin){
    this->trigPin = trigPin;
    this->echoPin = echoPin;
    pinMode(trigPin,OUTPUT);
    pinMode(echoPin,INPUT);
}

float SonarImpl::getDistance(){
    const float vs = 331.45 + 0.62*20;
    /* invio impulso */
    digitalWrite(trigPin,LOW);
    delayMicroseconds(3);
    digitalWrite(trigPin,HIGH);
    delayMicroseconds(5);
    digitalWrite(trigPin,LOW);
    
    /* ricevi l’eco */
    float tUS = pulseIn(echoPin, HIGH);
    float t = tUS / 1000.0 / 1000.0 / 2;
    float distance = t*vs;
    return distance;
}
