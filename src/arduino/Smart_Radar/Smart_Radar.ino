#include "Scheduler.h"
#include "Communicator.h"
#include "ScanTask.h"
#include "ManualScan.h"
#include "Pot.h"

#include <avr/wdt.h>

//PERIODS
#define PERIOD 10
#define SCAN_DELAY (timePot.getValue()/(180/ANGLE))/2

//PINS
#define MOTOR_PIN 4
#define TRIGGER_PIN 7
#define ECHO_PIN 8
#define LED_D_PIN 13
#define LED_A_PIN 12
#define PIR_PIN 2
#define POTENTIOMETER_PIN A0

//COSTANTS
#define ANGLE 11

Scheduler scheduler;
Communicator comm;

Pot timePot(A0, 5000, 10000);

bool pirInterruptEnabled = false;
Task* InterruptedTask;

unsigned char mode = 255;   //0 = single, 1 = manual, 2 = auto

InData recvData = {0, LEFT, 0, 0};
OutData senData = {0, LEFT, 0, 0, 0};

void recvAndDecode(InData*, Communicator*);
void scheduleMode(byte);

void setup() {
  wdt_enable(WDTO_8S);
  comm.init(115200);
  scheduler.init(PERIOD);


  scheduleMode(2);  //inizio in modalità auto

  Serial.println("Initialized correctly");
  delay(100);

  attachInterrupt(digitalPinToInterrupt(PIR_PIN), pirInterrupt, RISING);
}

void loop() {
  wdt_reset();

  if (comm.available()) {
    recvAndDecode(&recvData, &comm);
    /*//test
      Serial.println("Ack");  //debug
      senData.mode = recvData.mode;
      comm.send(&senData);
    */
  }

  scheduler.run();

}

void recvAndDecode(InData* data, Communicator* comm) {
  comm->read(data);
  if (mode != data->mode) {
    Serial.print("Changing mode... ");
    mode = data->mode;
    //cambia i task
    scheduler.clearTasks();
    pirInterruptEnabled = false;

    //schedula nuovi task
    scheduleMode(mode);

    timePot.setMin(data->Tmin);
    timePot.setMax(data->Tmax);
  }
}

void scheduleMode(unsigned char n) {
  Task* scanTaskSingle;
  Task* scanTaskManual;
  Task* scanTaskAuto;
  
  switch (n) {
    case 0:
      Serial.println("Single");
      delay(5);
      scanTaskSingle = new ScanTask( &comm, MOTOR_PIN, ANGLE, TRIGGER_PIN, ECHO_PIN, false);
      InterruptedTask = scanTaskSingle;
      scanTaskSingle->init(SCAN_DELAY);
      scheduler.addTask(scanTaskSingle);
      scanTaskSingle->setActive(false);
      pirInterruptEnabled = true;
      break;

    case 1:
      Serial.println("Manual");
      delay(5);
      scanTaskManual = new ManualScan( &comm, &recvData, MOTOR_PIN, ANGLE, TRIGGER_PIN, ECHO_PIN, true);
      scanTaskManual->init(SCAN_DELAY);
      scheduler.addTask(scanTaskManual);
      break;

    case 2:
      Serial.println("Auto");
      delay(5);
      scanTaskAuto = new ScanTask( &comm, MOTOR_PIN, ANGLE, TRIGGER_PIN, ECHO_PIN, true);
      scanTaskAuto->init(SCAN_DELAY);
      scheduler.addTask(scanTaskAuto);
      break;
    default:
      Serial.println("Error selecting mode");
  }
}

void pirInterrupt() {
  if (pirInterruptEnabled) {
    InterruptedTask->setActive(true);
    pirInterruptEnabled = false;
  }
}
