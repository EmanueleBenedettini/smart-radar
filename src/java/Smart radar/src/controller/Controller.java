package controller;

import java.util.ArrayList;
import java.util.List;

import model.msg.CommChannel;
import model.msg.SerialCommChannel;
import model.serial.DetectSerialPorts;
import view.ControlScreen;
import view.LaunchScreen;
import view.Screen;

public class Controller {
	
	public static final int BAUD_RATE = 115200;
	private CommChannel channel;
	
	public void init() {
		List<Integer> commSpeed = new ArrayList<>();
		
		//commSpeed.add(9600);
		commSpeed.add(115200);
		
		Screen screen = new LaunchScreen(DetectSerialPorts.getPorts(), commSpeed);
		while(true) {
			if(!screen.actionAvailable()) {
				break;
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		try {
			channel = new SerialCommChannel(screen.getLastAction(), BAUD_RATE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		screen = new ControlScreen(channel);
		
		try {
			while(true) {
				if(channel.isMsgAvailable()) {
					((ControlScreen) screen).append(channel.receiveMsg());
				}else {
					Thread.sleep(100);
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.close();
	}
	
	public void close() {
		((SerialCommChannel) channel).close();
	}
}
