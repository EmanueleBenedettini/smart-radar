package model.serial;

import java.util.ArrayList;
import java.util.List;
import jssc.*;

public class DetectSerialPorts {

	public static List<String> getPorts() {
		
		List<String> portNames = new ArrayList<>();
		String[] tmp = SerialPortList.getPortNames();
		for (int i = 0; i < tmp.length; i++){
		    portNames.add(tmp[i]);
		}
		return portNames;
	}

}
