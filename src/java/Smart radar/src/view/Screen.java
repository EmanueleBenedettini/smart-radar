package view;

public interface Screen {
	
	boolean actionAvailable();
	
	String getLastAction();
}
