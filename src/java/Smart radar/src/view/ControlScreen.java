package view;

import javax.swing.*;

import model.msg.CommChannel;

import java.awt.*;
import java.awt.event.ActionListener;

public class ControlScreen extends JFrame implements Screen {

	private static final long serialVersionUID = 1L;
	
	private CommChannel channel;
	
    private JButton single;
    private JButton manual;
    private JButton auto;
    private JButton left;
    private JButton right;
    
    private SpinnerModel modelMin = new SpinnerNumberModel(5, 0.5, 45, 0.5);  
    private SpinnerModel modelMax = new SpinnerNumberModel(10, 1, 60, 0.5); 
    
    private TextArea textArea;
    
    public ControlScreen(CommChannel channel) {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(300, 40);
        
        this.channel = channel;
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            //azione
            this.channel.sendMsg(bt.getText());
        };
        
        JPanel panel = new JPanel(new FlowLayout());
        
        this.single = new JButton("single");
        single.addActionListener(al);
        panel.add(this.single);
        
        this.manual = new JButton("manual");
        manual.addActionListener(al);
        panel.add(this.manual);
        
        this.auto = new JButton("auto");
        auto.addActionListener(al);
        panel.add(this.auto);
        
        this.left = new JButton("<");
        left.addActionListener(al);
        panel.add(this.left);
        
        this.right = new JButton(">");
        right.addActionListener(al);
        panel.add(this.right);
        
        JLabel tMinLabel = new JLabel("Tmin");
        JSpinner tMin = new JSpinner(modelMin);
        modelMin.addChangeListener((e)->{
            if(Double.valueOf(modelMax.getValue().toString()) <= Double.valueOf(modelMin.getValue().toString())) {
            	modelMin.setValue(modelMax.getPreviousValue());
            }
            channel.sendMsg("Tmin " + modelMin.getValue().toString());
        });
        panel.add(tMinLabel);
        panel.add(tMin);
        
        JLabel tMaxLabel = new JLabel("Tmax");
        JSpinner tMax = new JSpinner(modelMax);
        modelMax.addChangeListener((e)->{
            if(Double.valueOf(modelMin.getValue().toString()) >= Double.valueOf(modelMax.getValue().toString())) {
            	modelMax.setValue(modelMin.getNextValue());
            }
            channel.sendMsg("Tmax " + modelMax.getValue().toString());
        });
        panel.add(tMaxLabel);
        panel.add(tMax);
        
        this.textArea = new TextArea();
        this.textArea.setEditable(false);
        panel.add(this.textArea);
        
        
        this.getContentPane().add(BorderLayout.CENTER,panel);
        this.pack();
        this.setVisible(true);
    }
    
    public void append(String s) {
    	textArea.append(s + '\n');
    }
    
    public boolean actionAvailable() {
		return true;
    }
    
    public String getLastAction() {
    	return null;
    }
}
