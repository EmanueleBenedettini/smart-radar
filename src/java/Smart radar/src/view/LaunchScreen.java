package view;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

public class LaunchScreen extends JFrame implements Screen {
	private static final long serialVersionUID = -6218820567019985015L;

    private JComboBox speedSelector;
    private boolean active = true;
    
    private String selPort = "";
    
    public LaunchScreen(List<String> ports, List<Integer> speeds) {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(300, 40);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            active = false;
            selPort = bt.getText();
            this.setVisible(false);
        };
        
        JPanel panel = new JPanel(new FlowLayout());
        
        for(String s: ports){
        	JButton bt = new JButton();
        	bt.setText(s);
        	bt.addActionListener(al);
        	panel.add(bt);
        }
        
        this.speedSelector = new JComboBox();
        for(Integer i: speeds) {
        	this.speedSelector.addItem(i);
        }
        panel.add(this.speedSelector);
        
        this.getContentPane().add(BorderLayout.CENTER,panel);
        this.pack();
        this.setVisible(true);
    }
    
    public boolean actionAvailable() {
		return active;
    }
    
    public String getLastAction() {
    	return selPort;
    }
    
}